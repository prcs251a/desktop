/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prcsdesktop;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;

/**
 *
 * @author eli
 */
public class OracleConnector {
    
    private Connection conn;

    void createConnection() {
        try {
            conn = DriverManager.getConnection(
                    "jdbc:oracle:thin:@larry.uopnet.plymouth.ac.uk:1521:orcl",
                    "PRCS251A", "DamnDaniel"
            );
        } catch (SQLException ex) {
            Logger.getLogger(OracleConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void closeConnection() {
        try {
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(OracleConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    void testQuery() {
    try {
            Statement stmt = conn.createStatement();
            String query = "SELECT * FROM \"PRCS251A\".\"CUSTOMER\"";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                String txt = rs.getString("c1");
                System.out.println(txt);
                txt = rs.getString("c2");
                System.out.println(txt);
            }
        } catch (SQLException ex) {
            Logger.getLogger(OracleConnector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public DefaultListModel getUsers() {
        DefaultListModel returned = new DefaultListModel();
        try {
            Statement stmt = conn.createStatement();
            String query = "SELECT NAME FROM \"PRCS251A\".\"ROLES\"";
            ResultSet rs = stmt.executeQuery(query);
            List RoleList = new ArrayList();
            while (rs.next()) {
                RoleList.add(rs.getString("NAME"));
            }
            query = "SELECT * FROM \"PRCS251A\".\"USERS\"";
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                String role = RoleList.get(rs.getInt("ROLE_ID")).toString();
                String txt = "ID: " + rs.getString("USER_ID")  
                        + " | Username: " + rs.getString("USERNAME")  
                        + " | E-Mail: " + rs.getString("EMAIL") 
                        + " | Role: " + role;
                returned.addElement(txt);
            }
            return returned;
        } catch (SQLException ex) {
            returned.addElement(ex);
            return returned;
        }
    }
    
    public DefaultListModel getVenues() {
        DefaultListModel returned = new DefaultListModel();
        try {
            Statement stmt = conn.createStatement();
            String query = "SELECT * FROM \"PRCS251A\".\"VENUE\"";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                String txt = " ID: " + rs.getString("VENUE_ID")
                        + " Name: " + rs.getString("VENUENAME");
                returned.addElement(txt);
            }
            return returned;
        } catch (SQLException ex) {
            returned.addElement(ex);
            return returned;
        }
    }
    
    public DefaultListModel getArtists() {
        DefaultListModel returned = new DefaultListModel();
        try {
            Statement stmt = conn.createStatement();
            String query = "SELECT * FROM \"PRCS251A\".\"ARTIST\"";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                String txt = " ID: " + rs.getString("ARTIST_ID")
                        + " Name: " + rs.getString("ARTISTNAME");
                returned.addElement(txt);
            }
            return returned;
        } catch (SQLException ex) {
            returned.addElement(ex);
            return returned;
        }
    }
}
